// respostas dos formulários
var envioContato = function(){
	$.post('enviaContatoVisitantes', {
		nome : $('#contatoVisitantesNome').val(),
		email : $('#contatoVisitantesEmail').val(),
		assunto : $('#contatoVisitantesAssunto').val(),
		mensagem : $('#contatoVisitantesMensagem').val()
	}, function(resposta){console.log(resposta)});
    $('#contato .formulario').fadeOut('slow', function(){
        $(this).next().fadeIn('slow').parent().parent().find('.icone-contato').addClass('active');
    });
    return false;
};

var envioContatoComerciante = function(){
	$.post('enviaContatoEmpresa', {
		responsavel : $('#contatoEmpresaResponsavel').val(),
		email : $('#contatoEmpresaEmail').val(),
		empresa : $('#contatoEmpresaEmpresa').val(),
		atuacao : $('#contatoEmpresaAtuacao').val(),
		ddd : $('#contatoEmpresaDdd').val(),
		telefone : $('#contatoEmpresaTelefone').val(),
		informacoes : $('#contatoEmpresaInformacoes').val()
	}, function(resposta){console.log(resposta)});
    $('.overlay-comerciante-branco .formulario').fadeOut('slow', function(){
        $(this).next().fadeIn('slow');
    });
    return false;
};

var envioNewsletter = function(){
	$.post('cadastraNewsletter', {
		nome : $('#newsletterNome').val(),
		email : $('#newsletterEmail').val()
	}, function(retorno){
	    $('#newsletter .formulario').fadeOut('slow', function(){
	        $(this).next().html(retorno).fadeIn('slow');
	    });		
	});
    return false;
};


$(document).ready(function(){

    // smooth scroll on click
    $("#menu a").click(function(event){
        if($(this).hasClass('login')) {
            return;
        }

        event.preventDefault();
        $('html,body').animate({scrollTop:$(this.hash).offset().top - (($(this.hash).selector == "#contato") ? 165 : 110) }, 'slow');

    });



    // pega todas hashs do menu menos do login
    var navLinks  = $("#menu nav a"),
        hashArray = [],
        i         = navLinks.length;

    while (i--) {
        var navLink = $(navLinks[i]);
        if(!navLink.hasClass('login')) {
            hashArray.push(navLink.attr('href'));
        }
    }



    // verifica section ativa no scroll
    var hashTotal = hashArray.length;
    $(window).scroll(function(){
        var windowPos = $(window).scrollTop() + 120,
            i         = hashTotal;

        while (i--) {
            sectionID = hashArray[i];

            var sectionPos = $(sectionID).offset().top - (sectionID == "#contato" ? 50 : 0);
            var sectionHeight = $(sectionID).height() + (sectionID == "#contato" ? $(window).height() : 0);

            if (windowPos >= sectionPos && windowPos < (sectionPos + sectionHeight)) {
                $("a[href='" + sectionID + "']").addClass("active");
            } else {
                $("a[href='" + sectionID + "']").removeClass("active");
            }
        }

    });



    // slide como funciona
    $('.como-funciona-conteudo').hide();
    $('.como-funciona-slider .botao').on('click', function(event){
        event.preventDefault();
        var botao    = $(this),
            conteudo = botao.prev();

        conteudo.slideToggle('slow', function(){
            if(conteudo.is(':visible')) {
                botao.addClass('active');
                $('html,body').animate({ scrollTop: conteudo.offset().top - 110 }, 'slow');
            } else {
                botao.removeClass('active');
            }
        });
    });



    // validação ddd+telefone
    var fields = $('.overlay-comerciante-branco .number');
    fields.on('input', function(){
        var nonDigit = /\D/g;
        if (nonDigit.test(this.value)) {
            this.value = this.value.replace(nonDigit, '');
        }
    });

    $(document).on('click', '.abreOverlay', function(e){
    	e.preventDefault();

    	// destino do link
    	var destino = $(this).attr('href');

    	// DIV de fundo verde loadingCanvas
    	var loading = $("<div></div>").attr('id', 'loadingCanvas').css({
    		width : $('html').width(),
    		height : $('html').height()    		
    	});

    	// sobrepor página atual com loadingCanvas
    	$('html body').prepend(loading);
    	$('#loadingCanvas').css({'opacity' : 1});

    	// scroll até o topo
    	$('html, body').animate({'scrollTop' : 0}, 600);

    	// após o scroll esconder todos os elementos da página original
    	setTimeout( function(){
    		$('body #menu, body #newsletter, body section, body footer').css({ display: 'none'});
    	}, 650);

    	// buscar página destino via ajax
    	$.post(destino, {}, function(retorno){

    		// desmarca o item ativo do menu
	    	$("nav a.active").removeClass("active");

	    	// Retorno em HTML para objeto jquery
    		pagina = $(retorno);

    		// Esconde o conteúdo para criar o efeito de transição
    		pagina.filter('.overlay').css('opacity', 0);

    		// Injeta o conteúdo na página nova
    		$('#loadingCanvas').html(pagina);
	    	
	    	// Mostra o conteúdo
	    	setTimeout( function(){
	    		$('.overlay').css({'opacity' : 1});
	    	}, 150);
    	});
    });

    $(document).on('click', '.fechaOverlay', function(e){
    	e.preventDefault();
    	
    	$('html, body').animate({'scrollTop' : 0}, 300);
    	$('#loadingCanvas').css({'opacity' : 0});
    	$('body #menu, body #newsletter, body section, body footer').css({display: 'block'});
    	$("nav a.active").removeClass("active");
    	
    	setTimeout( function(){
    		$('#loadingCanvas').remove();
    	}, 300);
    });

});