<?php

Route::any('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::any('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::any('comerciante', array('as' => 'comerciante', 'uses' => 'HomeController@comerciante'));
Route::any('termos-de-uso', array('as' => 'termos', 'uses' => 'HomeController@termosDeUso'));
Route::post('enviaContatoVisitantes', array('as' => 'contato.visitantes', 'uses' => 'HomeController@enviaContatoVisitantes'));
Route::post('enviaContatoEmpresa', array('as' => 'contato.empresas', 'uses' => 'HomeController@enviaContatoEmpresa'));
Route::post('cadastraNewsletter', array('as' => 'cadastro.newsletter', 'uses' => 'HomeController@cadastraNewsletter'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{

    Route::get('download', function(){

    	$filename = 'cadastros_'.date('d-m-Y-H-i-s');

		Excel::create($filename, function($excel) {
		    $excel->sheet('Usuários Cadastrados', function($sheet) {
		        $sheet->fromModel(\CadastrosNewsletter::select('nome', 'email')->orderBy('nome')->get());
		    });
		})->download('csv');
    });
    	
    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('banner', 'Painel\BannerController');
	Route::resource('sobre', 'Painel\SobreController');
	Route::resource('comofunciona', 'Painel\ComoFuncionaController');
	Route::resource('vantagens', 'Painel\VantagensController');
	Route::resource('comerciante', 'Painel\ComercianteController');
	Route::resource('pelasuaempresa', 'Painel\PelaSuaEmpresaController');
	Route::resource('pelasuaempresavantagens', 'Painel\PelaSuaEmpresaVantagensController');
	Route::resource('contato', 'Painel\ContatoController');
	Route::resource('termosdeuso', 'Painel\TermosDeUsoController');
	Route::resource('contatosvisitantes', 'Painel\ContatosVisitantesController');
	Route::resource('contatosempresas', 'Painel\ContatosEmpresasController');
	Route::resource('cadastrosnewsletter', 'Painel\CadastrosNewsletterController');
	Route::resource('seo', 'Painel\SEOController');
	Route::resource('links', 'Painel\LinksController');
	//NOVASROTASDOPAINEL//
});
