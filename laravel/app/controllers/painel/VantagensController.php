<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Vantagens;

class VantagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.vantagens.index')->with('registros', Vantagens::all())->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.vantagens.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Vantagens;

		$object->frase_principal = Input::get('frase_principal');
		$object->frase_secundaria = Input::get('frase_secundaria');
		$object->feature1 = Input::get('feature1');
		$object->feature2 = Input::get('feature2');
		$object->feature3 = Input::get('feature3');
		$object->feature4 = Input::get('feature4');
		$object->feature5 = Input::get('feature5');
		$object->feature6 = Input::get('feature6');
		
		if($this->limiteInsercao && sizeof( Vantagens::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.vantagens.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.vantagens.edit')->with('registro', Vantagens::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Vantagens::find($id);

		$object->frase_principal = Input::get('frase_principal');
		$object->frase_secundaria = Input::get('frase_secundaria');
		$object->feature1 = Input::get('feature1');
		$object->feature2 = Input::get('feature2');
		$object->feature3 = Input::get('feature3');
		$object->feature4 = Input::get('feature4');
		$object->feature5 = Input::get('feature5');
		$object->feature6 = Input::get('feature6');
		
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.vantagens.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Vantagens::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.vantagens.index');
	}

}