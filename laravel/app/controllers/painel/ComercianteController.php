<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, Comerciante;

class ComercianteController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.comerciante.index')->with('registros', Comerciante::all())->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.comerciante.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Comerciante;

		$object->titulo = Input::get('titulo');
		$object->beneficio1 = Input::get('beneficio1');
		$object->beneficio2 = Input::get('beneficio2');
		$object->beneficio3 = Input::get('beneficio3');
		$object->beneficio4 = Input::get('beneficio4');
		$object->beneficio5 = Input::get('beneficio5');
		$object->texto_botao = Input::get('texto_botao');


		if($this->limiteInsercao && sizeof( Comerciante::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.comerciante.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.comerciante.edit')->with('registro', Comerciante::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Comerciante::find($id);

		$object->titulo = Input::get('titulo');
		$object->beneficio1 = Input::get('beneficio1');
		$object->beneficio2 = Input::get('beneficio2');
		$object->beneficio3 = Input::get('beneficio3');
		$object->beneficio4 = Input::get('beneficio4');
		$object->beneficio5 = Input::get('beneficio5');
		$object->texto_botao = Input::get('texto_botao');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.comerciante.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Comerciante::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.comerciante.index');
	}

}