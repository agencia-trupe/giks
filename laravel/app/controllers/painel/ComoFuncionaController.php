<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ComoFunciona;

class ComoFuncionaController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.comofunciona.index')->with('registros', ComoFunciona::all())->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.comofunciona.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ComoFunciona;

		$object->frase_principal = Input::get('frase_principal');
		$object->frase_secundaria = Input::get('frase_secundaria');
		$object->texto_coluna_esq = Input::get('texto_coluna_esq');
		$object->texto_coluna_dir = Input::get('texto_coluna_dir');
		$object->passo1 = Input::get('passo1');
		$object->passo2 = Input::get('passo2');
		$object->passo3 = Input::get('passo3');
		$object->passo4 = Input::get('passo4');


		if($this->limiteInsercao && sizeof( ComoFunciona::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.comofunciona.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.comofunciona.edit')->with('registro', ComoFunciona::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ComoFunciona::find($id);

		$object->frase_principal = Input::get('frase_principal');
		$object->frase_secundaria = Input::get('frase_secundaria');
		$object->texto_coluna_esq = Input::get('texto_coluna_esq');
		$object->texto_coluna_dir = Input::get('texto_coluna_dir');
		$object->passo1 = Input::get('passo1');
		$object->passo2 = Input::get('passo2');
		$object->passo3 = Input::get('passo3');
		$object->passo4 = Input::get('passo4');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.comofunciona.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ComoFunciona::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.comofunciona.index');
	}

}