<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ContatosVisitantes;

class ContatosVisitantesController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.contatosvisitantes.index')->with('registros', ContatosVisitantes::paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.contatosvisitantes.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ContatosVisitantes;

		$object->nome = Input::get('nome');
		$object->email = Input::get('email');
		$object->assunto = Input::get('assunto');
		$object->mensagem = Input::get('mensagem');



		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Contato criado com sucesso.');
			return Redirect::route('painel.contatosvisitantes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Contato!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.contatosvisitantes.edit')->with('registro', ContatosVisitantes::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ContatosVisitantes::find($id);

		$object->nome = Input::get('nome');
		$object->email = Input::get('email');
		$object->assunto = Input::get('assunto');
		$object->mensagem = Input::get('mensagem');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Contato alterado com sucesso.');
			return Redirect::route('painel.contatosvisitantes.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Contato!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ContatosVisitantes::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Contato removido com sucesso.');

		return Redirect::route('painel.contatosvisitantes.index');
	}

}