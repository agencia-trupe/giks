<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ContatosEmpresas;

class ContatosEmpresasController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.contatosempresas.index')->with('registros', ContatosEmpresas::paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.contatosempresas.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ContatosEmpresas;

		$object->responsavel = Input::get('responsavel');
		$object->email = Input::get('email');
		$object->string = Input::get('string');
		$object->atuacao = Input::get('atuacao');
		$object->telefone = Input::get('telefone');
		$object->mensagem = Input::get('mensagem');



		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Contato criado com sucesso.');
			return Redirect::route('painel.contatosempresas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Contato!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.contatosempresas.edit')->with('registro', ContatosEmpresas::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ContatosEmpresas::find($id);

		$object->responsavel = Input::get('responsavel');
		$object->email = Input::get('email');
		$object->string = Input::get('string');
		$object->atuacao = Input::get('atuacao');
		$object->telefone = Input::get('telefone');
		$object->mensagem = Input::get('mensagem');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Contato alterado com sucesso.');
			return Redirect::route('painel.contatosempresas.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Contato!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ContatosEmpresas::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Contato removido com sucesso.');

		return Redirect::route('painel.contatosempresas.index');
	}

}