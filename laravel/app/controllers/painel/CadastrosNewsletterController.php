<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, CadastrosNewsletter;

class CadastrosNewsletterController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cadastrosnewsletter.index')->with('registros', CadastrosNewsletter::paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cadastrosnewsletter.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new CadastrosNewsletter;

		$object->nome = Input::get('nome');
		$object->email = Input::get('email');



		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cadastro criado com sucesso.');
			return Redirect::route('painel.cadastrosnewsletter.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cadastro!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cadastrosnewsletter.edit')->with('registro', CadastrosNewsletter::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = CadastrosNewsletter::find($id);

		$object->nome = Input::get('nome');
		$object->email = Input::get('email');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cadastro alterado com sucesso.');
			return Redirect::route('painel.cadastrosnewsletter.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cadastro!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = CadastrosNewsletter::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cadastro removido com sucesso.');

		return Redirect::route('painel.cadastrosnewsletter.index');
	}

}