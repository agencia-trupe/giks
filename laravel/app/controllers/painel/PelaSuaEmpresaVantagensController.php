<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, PelaSuaEmpresaVantagens;

class PelaSuaEmpresaVantagensController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.pelasuaempresavantagens.index')->with('registros', PelaSuaEmpresaVantagens::orderBy('ordem', 'ASC')->get());		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.pelasuaempresavantagens.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new PelaSuaEmpresaVantagens;

		$object->texto = Input::get('texto');



		
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Vantagem criado com sucesso.');
			return Redirect::route('painel.pelasuaempresavantagens.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Vantagem!'));	

		}		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.pelasuaempresavantagens.edit')->with('registro', PelaSuaEmpresaVantagens::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = PelaSuaEmpresaVantagens::find($id);

		$object->texto = Input::get('texto');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Vantagem alterado com sucesso.');
			return Redirect::route('painel.pelasuaempresavantagens.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Vantagem!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = PelaSuaEmpresaVantagens::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Vantagem removido com sucesso.');

		return Redirect::route('painel.pelasuaempresavantagens.index');
	}

}