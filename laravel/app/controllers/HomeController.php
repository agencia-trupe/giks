<?php

use \Banner, \CadastrosNewsletter, \Comerciante, \ComoFunciona, \Contato, \ContatosEmpresas, \ContatosVisitantes, \Links;
use \PelaSuaEmpresa, \PelaSuaEmpresaVantagens, \SEO, \Sobre, \TermosDeUso, \Vantagens;

class HomeController extends Controller {

	public function index()
	{
		if(\Request::isMethod('post'))
			$view = "frontend.index";
		else
			$view = "frontend.templates.index";
		
		return   View::make($view)->with('banner', Banner::first())
								->with('sobre', Sobre::first())
								->with('comofunciona', ComoFunciona::first())
								->with('vantagens', Vantagens::first())
								->with('comerciante', Comerciante::first())
								->with('contato', Contato::first())
								->with('termosdeuso', TermosDeUso::first())
								->with('links', Links::first())
								->with('seo', SEO::first());
	}

	public function comerciante()
	{
		if(\Request::isMethod('post'))
			$view = "frontend.comerciante";
		else
			$view = "frontend.templates.comerciante";

		return View::make($view)->with('pelasuaempresa', PelaSuaEmpresa::first())
								->with('pelasuaempresavantagens', PelaSuaEmpresaVantagens::ordenado());
	}

	public function termosDeUso()
	{
		if(\Request::isMethod('post'))
			$view = "frontend.termos";
		else
			$view = "frontend.templates.termos";

		return View::make($view)->with('termos', TermosDeUso::first());
	}
	public function enviaContatoVisitantes()
	{
		$data['nome'] = htmlspecialchars(Input::get('nome'));
		$data['email'] = htmlspecialchars(Input::get('email'));
		$data['assunto'] = htmlspecialchars(Input::get('assunto'));
		$data['mensagem'] = htmlspecialchars(Input::get('mensagem'));
		
		if($data['nome'] && $data['email'] && $data['mensagem']){

			$contato = new ContatosVisitantes;
			$contato->nome = $data['nome'];
			$contato->email = $data['email'];
			$contato->assunto = $data['assunto'];
			$contato->mensagem = $data['mensagem'];
			$contato->save();

			$data['emailDestino'] = Contato::first()->email_contato_visitantes;			
			
			if($data['emailDestino']){
				\Mail::send('emails.contato-visitantes', $data, function($message) use ($data)
				{
				    $message->to($data['emailDestino'], 'Giks')
				    		->subject($data['assunto'])
				    		->replyTo($data['email'], $data['nome']);
				});
			}
		}
	}

	public function enviaContatoEmpresa()
	{
		$data['responsavel'] = htmlspecialchars(Input::get('responsavel'));
		$data['email'] = htmlspecialchars(Input::get('email'));
		$data['empresa'] = htmlspecialchars(Input::get('empresa'));
		$data['atuacao'] = htmlspecialchars(Input::get('atuacao'));
		$data['ddd'] = htmlspecialchars(Input::get('ddd'));
		$data['telefone'] = htmlspecialchars(Input::get('telefone'));
		$data['informacoes'] = htmlspecialchars(Input::get('informacoes'));

		if($data['responsavel'] && $data['email'] && $data['informacoes']){
			
			$contato = new ContatosEmpresas;
			$contato->responsavel = $data['responsavel'];
			$contato->email = $data['email'];
			$contato->empresa = $data['empresa'];
			$contato->atuacao = $data['atuacao'];
			$contato->telefone = $data['ddd'].' '.$data['telefone'];
			$contato->mensagem = $data['informacoes'];
			$contato->save();

			$data['emailDestino'] = Contato::first()->email_contato_empresas;			
			
			if($data['emailDestino']){
				\Mail::send('emails.contato-empresas', $data, function($message) use ($data)
				{
				    $message->to($data['emailDestino'], 'Giks')
				    		->subject('Contato via site')
				    		->replyTo($data['email'], $data['nome']);
				});
			}
		}
	}

	public function cadastraNewsletter()
	{
		$nome = htmlspecialchars(Input::get('nome'));
		$email = htmlspecialchars(Input::get('email'));
		if($email != ''){
			if(sizeof(CadastrosNewsletter::where('email', '=', $email)->get()) == 0){
				$cadastro = new CadastrosNewsletter;
				$cadastro->nome = $nome;
				$cadastro->email = $email;
				$cadastro->save();
				return "<h4>E-mail cadastrado com sucesso!</h4>";
			}else{
				return "<h4>O E-mail já está cadastrado</h4>";
			}
		}else{
			return "<h4>Informe seu E-mail!</h4>";
		}
	}
}
