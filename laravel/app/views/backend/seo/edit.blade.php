@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Informações
        </h2>  

		{{ Form::open( array('route' => array('painel.seo.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" >
				</div>
				
				<div class="form-group">
					<label for="inputDescrição">Descrição</label>
					<textarea name="descricao" class="form-control" id="inputDescrição" >{{$registro->descricao }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputKeywords">Keywords</label>
					<input type="text" class="form-control" id="inputKeywords" name="keywords" value="{{$registro->keywords}}" >
				</div>
				
				<div class="form-group">
					@if($registro->imagem_facebook)
						Imagem de Compartilhamento Atual<br>
						<img src="assets/img/seo/{{$registro->imagem_facebook}}"><br>
					@endif
					<label for="inputImagem de Compartilhamento">Trocar Imagem de Compartilhamento</label>
					<input type="file" class="form-control" id="inputImagem de Compartilhamento" name="imagem_facebook">
				</div>
				

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.seo.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop