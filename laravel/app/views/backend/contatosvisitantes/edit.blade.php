@section('conteudo')

    <div class="container add">

      	<h2>
        	Visualizar Contato
        </h2>  

		<div class="pad">

			<div class="form-group">
				<label for="inputNome">Nome</label>
				<input type="text" class="form-control" disabled id="inputNome" name="nome" value="{{$registro->nome}}" >
			</div>
			
			<div class="form-group">
				<label for="inputEmail">Email</label>
				<input type="text" class="form-control" disabled id="inputEmail" name="email" value="{{$registro->email}}" >
			</div>
			
			<div class="form-group">
				<label for="inputAssunto">Assunto</label>
				<input type="text" class="form-control" disabled id="inputAssunto" name="assunto" value="{{$registro->assunto}}" >
			</div>
			
			<div class="form-group">
				<label for="inputMensagem">Mensagem</label>
				<div class="well">{{nl2br($registro->mensagem) }}</div>
			</div>
			

			<a href="{{URL::route('painel.contatosvisitantes.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</div>
    </div>
    
@stop