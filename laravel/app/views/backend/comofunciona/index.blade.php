@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Como Funciona 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Frase Principal</th>
				<th>Frase Secundária</th>
				<th>Texto (Coluna Esquerda)</th>
				<th>Texto (Coluna Direita)</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->frase_principal }}</td>
				<td>{{ $registro->frase_secundaria }}</td>
				<td>{{ Str::words(strip_tags($registro->texto_coluna_esq), 15) }}</td>
				<td>{{ Str::words(strip_tags($registro->texto_coluna_dir), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.comofunciona.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop