@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.comofunciona.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputFrase Principal">Frase Principal</label>
					<input type="text" class="form-control" id="inputFrase Principal" name="frase_principal" value="{{$registro->frase_principal}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputFrase Secundária">Frase Secundária</label>
					<input type="text" class="form-control" id="inputFrase Secundária" name="frase_secundaria" value="{{$registro->frase_secundaria}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto (Coluna Esquerda)">Texto (Coluna Esquerda)</label>
					<textarea name="texto_coluna_esq" class="form-control" id="inputTexto (Coluna Esquerda)" >{{$registro->texto_coluna_esq }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto (Coluna Direita)">Texto (Coluna Direita)</label>
					<textarea name="texto_coluna_dir" class="form-control" id="inputTexto (Coluna Direita)" >{{$registro->texto_coluna_dir }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 1">Passo 1</label>
					<textarea name="passo1" class="form-control" id="inputPasso 1" >{{$registro->passo1 }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 2">Passo 2</label>
					<textarea name="passo2" class="form-control" id="inputPasso 2" >{{$registro->passo2 }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 3">Passo 3</label>
					<textarea name="passo3" class="form-control" id="inputPasso 3" >{{$registro->passo3 }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 4">Passo 4</label>
					<textarea name="passo4" class="form-control" id="inputPasso 4" >{{$registro->passo4 }}</textarea>
				</div>
				

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.comofunciona.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop