@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.comofunciona.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputFrase Principal">Frase Principal</label>
					<input type="text" class="form-control" id="inputFrase Principal" name="frase_principal" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['frase_principal'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputFrase Secundária">Frase Secundária</label>
					<input type="text" class="form-control" id="inputFrase Secundária" name="frase_secundaria" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['frase_secundaria'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto (Coluna Esquerda)">Texto (Coluna Esquerda)</label>
					<textarea name="texto_coluna_esq" class="form-control" id="inputTexto (Coluna Esquerda)" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_coluna_esq'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto (Coluna Direita)">Texto (Coluna Direita)</label>
					<textarea name="texto_coluna_dir" class="form-control" id="inputTexto (Coluna Direita)" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_coluna_dir'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 1">Passo 1</label>
					<textarea name="passo1" class="form-control" id="inputPasso 1" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['passo1'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 2">Passo 2</label>
					<textarea name="passo2" class="form-control" id="inputPasso 2" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['passo2'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 3">Passo 3</label>
					<textarea name="passo3" class="form-control" id="inputPasso 3" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['passo3'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputPasso 4">Passo 4</label>
					<textarea name="passo4" class="form-control" id="inputPasso 4" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['passo4'] }} @endif</textarea>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.comofunciona.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop