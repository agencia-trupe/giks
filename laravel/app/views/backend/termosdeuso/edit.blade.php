@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.termosdeuso.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTextoE">Texto (Coluna Esquerda)</label>
					<textarea name="texto_coluna_esq" class="form-control" id="inputTextoE" >{{$registro->texto_coluna_esq }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTextoD">Texto (Coluna Direita)</label>
					<textarea name="texto_coluna_dir" class="form-control" id="inputTextoD" >{{$registro->texto_coluna_dir }}</textarea>
				</div>

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.termosdeuso.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop