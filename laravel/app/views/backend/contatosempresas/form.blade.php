@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Contato
        </h2>  

		<form action="{{URL::route('painel.contatosempresas.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputResponsável">Responsável</label>
					<input type="text" class="form-control" id="inputResponsável" name="responsavel" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['responsavel'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="text" class="form-control" id="inputEmail" name="email" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['email'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" id="inputEmpresa" name="string" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['string'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputAtuação">Atuação</label>
					<input type="text" class="form-control" id="inputAtuação" name="atuacao" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['atuacao'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['telefone'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputMensagem">Mensagem</label>
					<input type="text" class="form-control" id="inputMensagem" name="mensagem" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['mensagem'] }}" @endif >
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.contatosempresas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop