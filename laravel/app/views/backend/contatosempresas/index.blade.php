@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Contatos Enviados - Empresas
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Responsável</th>
				<th>Email</th>
				<th>Empresa</th>
				<th>Atuação</th>
				<th>Telefone</th>
				<th>Mensagem</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->responsavel }}</td>
				<td>{{ $registro->email }}</td>
				<td>{{ $registro->empresa }}</td>
				<td>{{ $registro->atuacao }}</td>
				<td>{{ $registro->telefone }}</td>
				<td>{{ Str::words(strip_tags($registro->mensagem), 15) }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.contatosempresas.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>
					{{ Form::open(array('route' => array('painel.contatosempresas.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop