@section('conteudo')

    <div class="container add">

      	<h2>
        	Visualizar Contato
        </h2>  

			<div class="pad">

				<div class="form-group">
					<label for="inputResponsável">Responsável</label>
					<input type="text" class="form-control" disabled id="inputResponsável" name="responsavel" value="{{$registro->responsavel}}" >
				</div>
				
				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="text" class="form-control" disabled id="inputEmail" name="email" value="{{$registro->email}}" >
				</div>
				
				<div class="form-group">
					<label for="inputEmpresa">Empresa</label>
					<input type="text" class="form-control" disabled id="inputEmpresa" name="empresa" value="{{$registro->empresa}}" >
				</div>
				
				<div class="form-group">
					<label for="inputAtuação">Atuação</label>
					<input type="text" class="form-control" disabled id="inputAtuação" name="atuacao" value="{{$registro->atuacao}}" >
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" disabled id="inputTelefone" name="telefone" value="{{$registro->telefone}}" >
				</div>
				
				<div class="form-group">
					<label for="inputMensagem">Mensagem</label>
					<div class="well">{{nl2br($registro->mensagem)}}</div>
				</div>
				<a href="{{URL::route('painel.contatosempresas.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		
    </div>
    
@stop