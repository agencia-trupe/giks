@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.pelasuaempresa.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputDestaque">Destaque</label>
					<textarea name="destaque" class="form-control fundoVerde" id="inputDestaque" >{{$registro->destaque }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control fundoVerde" id="inputTexto" >{{$registro->texto }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputChamada do Formulário">Chamada do Formulário</label>
					<input type="text" name="chamada_formulario" class="form-control" id="inputChamada do Formulário" value="{{$registro->chamada_formulario }}">
				</div>
				

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.pelasuaempresa.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop