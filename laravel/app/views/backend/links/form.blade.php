@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Links
        </h2>  

		<form action="{{URL::route('painel.links.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputLink App Store">Link App Store</label>
					<input type="text" class="form-control" id="inputLink App Store" name="app_store" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['app_store'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputGoogle Play">Google Play</label>
					<input type="text" class="form-control" id="inputGoogle Play" name="google_play" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['google_play'] }}" @endif >
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.links.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop