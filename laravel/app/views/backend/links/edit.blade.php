@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Links
        </h2>  

		{{ Form::open( array('route' => array('painel.links.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputLink App Store">Link App Store</label>
					<input type="text" class="form-control" id="inputLink App Store" name="app_store" value="{{$registro->app_store}}" >
				</div>
				
				<div class="form-group">
					<label for="inputGoogle Play">Google Play</label>
					<input type="text" class="form-control" id="inputGoogle Play" name="google_play" value="{{$registro->google_play}}" >
				</div>
				

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.links.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop