@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Informações de Contato
        </h2>  

		<form action="{{URL::route('painel.contato.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputEmail de envio do Formulário (Visitantes)">Email de envio do Formulário (Visitantes)</label>
					<input type="text" class="form-control" id="inputEmail de envio do Formulário (Visitantes)" name="email_contato_visitantes" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['email_contato_visitantes'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputEmail de envio do Formulário (Empresas)">Email de envio do Formulário (Empresas)</label>
					<input type="text" class="form-control" id="inputEmail de envio do Formulário (Empresas)" name="email_contato_empresas" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['email_contato_empresas'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['facebook'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputTwitter">Twitter</label>
					<input type="text" class="form-control" id="inputTwitter" name="twitter" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['twitter'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputInstagram">Instagram</label>
					<input type="text" class="form-control" id="inputInstagram" name="instagram" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['instagram'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputGoogle+">Google+</label>
					<input type="text" class="form-control" id="inputGoogle+" name="google_plus" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['google_plus'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputYoutube">Youtube</label>
					<input type="text" class="form-control" id="inputYoutube" name="youtube" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['youtube'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputVimeo">Vimeo</label>
					<input type="text" class="form-control" id="inputVimeo" name="vimeo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['vimeo'] }}" @endif >
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop