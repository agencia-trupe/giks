@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Informações de Contato
        </h2>  

		{{ Form::open( array('route' => array('painel.contato.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputEmail de envio do Formulário (Visitantes)">Email de envio do Formulário (Visitantes)</label>
					<input type="text" class="form-control" id="inputEmail de envio do Formulário (Visitantes)" name="email_contato_visitantes" value="{{$registro->email_contato_visitantes}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputEmail de envio do Formulário (Empresas)">Email de envio do Formulário (Empresas)</label>
					<input type="text" class="form-control" id="inputEmail de envio do Formulário (Empresas)" name="email_contato_empresas" value="{{$registro->email_contato_empresas}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" value="{{$registro->facebook}}" >
				</div>
				
				<div class="form-group">
					<label for="inputTwitter">Twitter</label>
					<input type="text" class="form-control" id="inputTwitter" name="twitter" value="{{$registro->twitter}}" >
				</div>
				
				<div class="form-group">
					<label for="inputInstagram">Instagram</label>
					<input type="text" class="form-control" id="inputInstagram" name="instagram" value="{{$registro->instagram}}" >
				</div>
				
				<div class="form-group">
					<label for="inputGoogle+">Google+</label>
					<input type="text" class="form-control" id="inputGoogle+" name="google_plus" value="{{$registro->google_plus}}" >
				</div>
				
				<div class="form-group">
					<label for="inputYoutube">Youtube</label>
					<input type="text" class="form-control" id="inputYoutube" name="youtube" value="{{$registro->youtube}}" >
				</div>
				
				<div class="form-group">
					<label for="inputVimeo">Vimeo</label>
					<input type="text" class="form-control" id="inputVimeo" name="vimeo" value="{{$registro->vimeo}}" >
				</div>
				

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.contato.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop