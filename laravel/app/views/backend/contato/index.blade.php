@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Contato 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Emails de envio do Formulário</th>				
				<th>Mídias Sociais</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
				<td>
					{{ 'Visitantes: '.$registro->email_contato_visitantes.'<br>' }}
					{{ 'Empresas: '.$registro->email_contato_empresas }}
				</td>
				<td>
					@if($registro->facebook) <a class="btn btn-sm btn-default" href="{{$registro->facebook}}" style="margin-bottom:3px;" title="Facebook">Facebook</a><br> @endif
					@if($registro->twitter) <a class="btn btn-sm btn-default" href="{{$registro->twitter}}" style="margin-bottom:3px;" title="Twitter">Twitter</a><br> @endif
					@if($registro->instagram) <a class="btn btn-sm btn-default" href="{{$registro->instagram}}" style="margin-bottom:3px;" title="Instagram">Instagram</a><br> @endif
					@if($registro->google_plus) <a class="btn btn-sm btn-default" href="{{$registro->google_plus}}" style="margin-bottom:3px;" title="Google+">Google+</a><br> @endif
					@if($registro->youtube) <a class="btn btn-sm btn-default" href="{{$registro->youtube}}" style="margin-bottom:3px;" title="Youtube">Youtube</a><br> @endif
					@if($registro->vimeo) <a class="btn btn-sm btn-default" href="{{$registro->vimeo}}" style="margin-bottom:3px;" title="Vimeo">Vimeo</a> @endif
				</td>
				<td class="crud-actions">
                    <a href='{{ URL::route('painel.contato.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop