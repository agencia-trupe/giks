@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.vantagens.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputFrasePrincipal">Frase Principal</label>
					<input type="text" name="frase_principal" class="form-control" id="inputFrasePrincipal" value="{{$registro->frase_principal }}">
				</div>
				
				<div class="form-group">
					<label for="inputFraseSecundária">Frase Secundária</label>
					<input type="text" name="frase_secundaria" class="form-control" id="inputFraseSecundária" value="{{$registro->frase_secundaria }}">
				</div>

				<div class="well">
					<div class="form-group">
						<div style="width:62px; height:62px; margin-bottom:5px; border:1px #ddd solid; background: url(assets/img/geral-s74653a7d7b.png) -60px -249px no-repeat;"></div>
						<label for="inputFeature1">Texto para Feature 1</label>
						<input type="text" name="feature1" class="form-control" id="inputFeature1" value="{{$registro->feature1}}">
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:62px; height:62px; margin-bottom:5px; border:1px #ddd solid; background: url(assets/img/geral-s74653a7d7b.png) 0 -189px no-repeat;"></div>
						<label for="inputFeature2">Texto para Feature 2</label>
						<input type="text" name="feature2" class="form-control" id="inputFeature2" value="{{$registro->feature2}}">
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:62px; height:62px; margin-bottom:5px; border:1px #ddd solid; background: url(assets/img/geral-s74653a7d7b.png) -60px -189px no-repeat;"></div>
						<label for="inputFeature3">Texto para Feature 3</label>
						<input type="text" name="feature3" class="form-control" id="inputFeature3" value="{{$registro->feature3}}">
					</div>
				</div>

				<div class="well">
					<div class="form-group">
						<div style="width:62px; height:62px; margin-bottom:5px; border:1px #ddd solid; background: url(assets/img/geral-s74653a7d7b.png) -120px -189px no-repeat;"></div>
						<label for="inputFeature4">Texto para Feature 4</label>
						<input type="text" name="feature4" class="form-control" id="inputFeature4" value="{{$registro->feature4}}">
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:62px; height:62px; margin-bottom:5px; border:1px #ddd solid; background: url(assets/img/geral-s74653a7d7b.png) 0 -249px no-repeat;"></div>
						<label for="inputFeature5">Texto para Feature 5</label>
						<input type="text" name="feature5" class="form-control" id="inputFeature5" value="{{$registro->feature5}}">
					</div>
				</div>

				<div class="well">
					<div class="form-group">
						<div style="width:62px; height:62px; margin-bottom:5px; border:1px #ddd solid; background: url(assets/img/geral-s74653a7d7b.png) -120px -249px no-repeat;"></div>
						<label for="inputFeature 6">Texto para Feature 6</label>
						<input type="text" name="feature6" class="form-control" id="inputFeature6" value="{{$registro->feature6}}">
					</div>
				</div>

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.vantagens.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop