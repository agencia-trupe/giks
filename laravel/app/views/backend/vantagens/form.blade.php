@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.vantagens.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputFrase Principal">Frase Principal</label>
					<textarea name="frase_principal" class="form-control" id="inputFrase Principal" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['frase_principal'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputFrase Secundária">Frase Secundária</label>
					<textarea name="frase_secundaria" class="form-control" id="inputFrase Secundária" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['frase_secundaria'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputFeature 1">Feature 1</label>
					<textarea name="feature1" class="form-control" id="inputFeature 1" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['feature1'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputFeature 2">Feature 2</label>
					<textarea name="feature2" class="form-control" id="inputFeature 2" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['feature2'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputFeature 3">Feature 3</label>
					<textarea name="feature3" class="form-control" id="inputFeature 3" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['feature3'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputFeature 4">Feature 4</label>
					<textarea name="feature4" class="form-control" id="inputFeature 4" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['feature4'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputFeature 5">Feature 5</label>
					<textarea name="feature5" class="form-control" id="inputFeature 5" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['feature5'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputFeature 6">Feature 6</label>
					<textarea name="feature6" class="form-control" id="inputFeature 6" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['feature6'] }} @endif</textarea>
				</div>

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.vantagens.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop