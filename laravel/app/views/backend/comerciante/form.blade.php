@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.comerciante.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputBenefício1">Benefício 1</label>
					<input type="text" class="form-control" id="inputBenefício1" name="beneficio1" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['beneficio1'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputBenefício2">Benefício 2</label>
					<input type="text" class="form-control" id="inputBenefício2" name="beneficio2" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['beneficio2'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputBenefício3">Benefício 3</label>
					<input type="text" class="form-control" id="inputBenefício3" name="beneficio3" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['beneficio3'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputBenefício4">Benefício 4</label>
					<input type="text" class="form-control" id="inputBenefício4" name="beneficio4" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['beneficio4'] }}" @endif >
				</div>

				<div class="form-group">
					<label for="inputBenefício5">Benefício 5</label>
					<input type="text" class="form-control" id="inputBenefício5" name="beneficio5" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['beneficio5'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputTexto do Botão">Texto do Botão</label>
					<input type="text" class="form-control" id="inputTexto do Botão" name="texto_botao" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['texto_botao'] }}" @endif required>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.comerciante.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop