@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Comerciante 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Título</th>
				<th>Benefícios</th>
				<th>Texto do Botão</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->titulo }}</td>
				<td>
					@if($registro->beneficio1) {{ '&middot; '.$registro->beneficio1.'<br>' }} @endif
					@if($registro->beneficio2) {{ '&middot; '.$registro->beneficio2.'<br>' }} @endif
					@if($registro->beneficio3) {{ '&middot; '.$registro->beneficio3.'<br>' }} @endif
					@if($registro->beneficio4) {{ '&middot; '.$registro->beneficio4.'<br>' }} @endif
					@if($registro->beneficio5) {{ '&middot; '.$registro->beneficio5 }} @endif
				</td>
				<td>{{ $registro->texto_botao }}</td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.comerciante.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    
</div>

@stop