@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.comerciante.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" >
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:33px; height:33px; background:url('assets/img/geral-s74653a7d7b.png') #4adda8 -129px -81px no-repeat;"></div>
						<label for="inputBenefício1">Texto para Benefício 1</label>
						<input type="text" class="form-control" id="inputBenefício1" name="beneficio1" value="{{$registro->beneficio1}}" >
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:33px; height:33px; background:url('assets/img/geral-s74653a7d7b.png') #4adda8 -136px 0 no-repeat;"></div>
						<label for="inputBenefício2">Texto para Benefício 2</label>
						<input type="text" class="form-control" id="inputBenefício2" name="beneficio2" value="{{$registro->beneficio2}}" >
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:33px; height:33px; background:url('assets/img/geral-s74653a7d7b.png') #4adda8 -160px -643px no-repeat;"></div>
						<label for="inputBenefício3">Texto para Benefício 3</label>
						<input type="text" class="form-control" id="inputBenefício3" name="beneficio3" value="{{$registro->beneficio3}}" >
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:33px; height:33px; background:url('assets/img/geral-s74653a7d7b.png') #4adda8 -160px -723px no-repeat;"></div>
						<label for="inputBenefício4">Texto para Benefício 4</label>
						<input type="text" class="form-control" id="inputBenefício4" name="beneficio4" value="{{$registro->beneficio4}}" >
					</div>
				</div>
				
				<div class="well">
					<div class="form-group">
						<div style="width:33px; height:33px; background:url('assets/img/geral-s74653a7d7b.png') #4adda8 -96px -81px no-repeat;"></div>
						<label for="inputBenefício5">Texto para Benefício 5</label>
						<input type="text" class="form-control" id="inputBenefício5" name="beneficio5" value="{{$registro->beneficio5}}" >
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputTextodoBotão">Texto do Botão</label>
					<input type="text" class="form-control" id="inputTextodoBotão" name="texto_botao" value="{{$registro->texto_botao}}" required>
				</div>
				

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.comerciante.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop