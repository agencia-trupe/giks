@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Vantagem
        </h2>  

		{{ Form::open( array('route' => array('painel.pelasuaempresavantagens.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<input type="text" class="form-control" id="inputTexto" name="texto" value="{{$registro->texto}}" required>
				</div>
				

				<button type="submit" title="Salvar" class="btn btn-success">Salvar</button>

				<a href="{{URL::route('painel.pelasuaempresavantagens.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop