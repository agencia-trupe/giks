<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Giks - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/jquery-ui/themes/base/jquery-ui',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Giks</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li @if(str_is('painel.banner*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.banner.index')}}" title="Banner">Banner</a>
						</li>

						<li @if(str_is('painel.sobre*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.sobre.index')}}" title="Sobre">Sobre</a>
						</li>

						<li @if(str_is('painel.comofunciona*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.comofunciona.index')}}" title="Como Funciona">Como Funciona</a>
						</li>

						<li @if(str_is('painel.vantagens*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.vantagens.index')}}" title="Vantagens">Vantagens</a>
						</li>

						<li @if(str_is('painel.comerciante*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.comerciante.index')}}" title="Comerciante">Comerciante</a>
						</li>

						<li class="dropdown @if(str_is('painel.pelasuaempresa*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pela Sua Empresa <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.pelasuaempresa.index')}}" title="Pela Sua Empresa - Texto">Texto</a></li>
								<li><a href="{{URL::route('painel.pelasuaempresavantagens.index')}}" title="Pela Sua Empresa - Vantagens">Vantagens</a></li>
							</ul>
						</li>

						<li @if(str_is('painel.contato.index', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.contato.index')}}" title="Contato">Contato</a>
						</li>

						<li @if(str_is('painel.termosdeuso*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.termosdeuso.index')}}" title="Termos de Uso">Termos de Uso</a>
						</li>

						<li class="dropdown @if(str_is('painel.contatos*', Route::currentRouteName()) || str_is('painel.cadastros*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Engajamento <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.contatosvisitantes.index')}}" title="Contatos Enviados (Visitantes)">Contatos Enviados (Visitantes)</a></li>
								<li><a href="{{URL::route('painel.contatosempresas.index')}}" title="Contatos Enviados (Empresas)">Contatos Enviados (Empresas)</a></li>
								<li><a href="{{URL::route('painel.cadastrosnewsletter.index')}}" title="Cadastros na Newsletter">Cadastros na Newsletter</a></li>
							</ul>
						</li>

						<li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName()) || str_is('painel.links*', Route::currentRouteName()) || str_is('painel.seo*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.links.index')}}" title="Links para o App">Links para o App</a></li>
								<li><a href="{{URL::route('painel.seo.index')}}" title="SEO">SEO</a></li>
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários do Painel</a></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>
		
		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
