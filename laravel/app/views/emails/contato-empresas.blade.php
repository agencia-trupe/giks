<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Responsável : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $responsavel }}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Email : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Empresa : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Atuação : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $atuacao }}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Telefone : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $ddd.' '.$telefone }}</span><br>
<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mensagem : </span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $informacoes }}</span><br>
</body>
</html>
