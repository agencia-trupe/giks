<div class="overlay">
    <div class="centro">
        <a href="home#comerciante" class="fechaOverlay overlay-voltar">Voltar</a>
        <div class="overlay-conteudo overlay-comerciante">
            <h1>{{$pelasuaempresa->titulo}}</h1>
            <h2>{{Tools::removeTag('<p>', $pelasuaempresa->destaque)}}</h2>
            <div class="overlay-comerciante-texto">
                {{$pelasuaempresa->texto}}
            </div>
            <ul class="overlay-comerciante-topicos">
            	@if(sizeof($pelasuaempresavantagens))
            		@foreach($pelasuaempresavantagens as $vantagem)
            			<li>{{$vantagem->texto}}</li>
            		@endforeach
            	@endif                
            </ul>
        </div>
    </div>
</div>
<div class="overlay overlay-branco">
    <div class="centro">
        <div class="overlay-conteudo overlay-comerciante-branco">
            <div class="formulario">
                <h1>{{Tools::removeTag('<p>', $pelasuaempresa->chamada_formulario)}}</h1>
                <form action="" onsubmit="return envioContatoComerciante()">
                    <div class="esquerda">
                        <input name="responsavel" id="contatoEmpresaResponsavel" type="text" placeholder="Nome do responsável" required>
                        <input name="email" id="contatoEmpresaEmail" type="email" placeholder="E-mail" required>
                        <input name="empresa" id="contatoEmpresaEmpresa" type="text" placeholder="Nome da empresa" required>
                        <input name="atuacao" id="contatoEmpresaAtuacao" type="text" placeholder="Ramo de atuação" required>
                    </div>
                    <div class="direita">
                        <input name="ddd" id="contatoEmpresaDdd" type="text" placeholder="DDD" class="number" maxlength="3">
                        <input name="telefone" id="contatoEmpresaTelefone" type="text" placeholder="Telefone" class="number">
                        <textarea name="informacoes" id="contatoEmpresaInformacoes" placeholder="Informações adicionais"></textarea>
                    </div>
                    <div class="submit">
                        <input type="submit" value="Enviar mensagem">
                    </div>
                </form>
                <img src="assets/img/overlay_personagem.jpg" alt="">
            </div>
            <div class="response">
                <h5>Sua mensagem foi enviada com sucesso!</h5>
                <p>Em breve entraremos em contato.</p>
            </div>
        </div>
    </div>
</div>