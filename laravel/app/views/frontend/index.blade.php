<div id="menu">
    <div class="centro">
        <h1><a href="#intro">Giks</a></h1>
        <nav>
            <a href="#sobre">Sobre</a>
            <a href="#como-funciona">Como Funciona</a>
            <a href="#vantagens">Vantagens</a>
            <a href="#comerciante">Comerciante</a>
            <!--<a href="#quem-aderiu">Quem já aderiu</a>-->
            <a href="#contato">Contato</a>
            <a href="#" class="login">Login</a>
        </nav>
    </div>
</div>

<section id="intro">
    <div class="centro">
        <h2>{{ Tools::removeTag("<p>", $banner->texto)}}</h2>
        <div class="breve">
        	@if($links->google_play)
            	<a class="google-play" href="{{$links->google_play}}" target="_blank" title="Faça download no Google Play">Faça download no Google Play</a>
            @endif
            @if($links->app_store)
            	<a class="appstore" href="{{$links->app_store}}" target="_blank" title="Faça download na App Store">Faça download na App Store</a>
            @endif
        </div>
        <div class="iphone"></div>
    </div>
</section>

<section id="sobre">
    <div class="centro">
        <img src="assets/img/sobre_personagem.jpg" alt="">
        <div class="sobre-texto">{{$sobre->texto}}</div>
    </div>
</section>

<section id="como-funciona">
    <div class="como-funciona-chamada">
        <div class="centro">
            <h3>{{$comofunciona->frase_principal}}</h3>
            <h4>{{$comofunciona->frase_secundaria}}</h4>
        </div>
    </div>
    <div class="como-funciona-slider">
        <div class="centro">
            <div class="como-funciona-conteudo">
                <div class="esquerda">{{$comofunciona->texto_coluna_esq}}</div>
                <div class="direita">
                    {{$comofunciona->texto_coluna_dir}}
                    <div class="breve">
                        @if($links->google_play)
	                    	<a class="google-play" href="{{$links->google_play}}" target="_blank" title="Faça download no Google Play">Faça download no Google Play</a>
	                    @endif
	                    @if($links->app_store)
	                    	<a class="appstore" href="{{$links->app_store}}" target="_blank" title="Faça download na App Store">Faça download na App Store</a>
	                    @endif
                    </div>
                </div>
            </div>
            <a href="" class="botao"><span class="como-funciona-conteudo"></span></a>
        </div>
    </div>
    <div class="como-funciona-icones">
        <div class="centro">
            <ul>
                <li>
                    <div class="geral-comofunciona_passo1"></div>
                    <p>{{Tools::removeTag('<p>', $comofunciona->passo1)}}</p>
                </li>
                <li>
                    <div class="geral-comofunciona_passo2"></div>
                    <p>{{Tools::removeTag('<p>', $comofunciona->passo2)}}</p>
                </li>
                <li>
                    <div class="geral-comofunciona_passo3"></div>
                    <p>{{Tools::removeTag('<p>', $comofunciona->passo3)}}</p>
                </li>
                <li>
                    <div class="geral-comofunciona_passo4"></div>
                    <p>{{Tools::removeTag('<p>', $comofunciona->passo4)}}</p>
                </li>
            </ul>
        </div>
    </div>
</section>

<section id="vantagens">
    <div class="centro">
        <h3>{{$vantagens->frase_principal}}</h3>
        <h4>{{$vantagens->frase_secundaria}}</h4>
        <div class="iphone">
            <div class="esquerda">
                <div class="vantagens">
                    <div class="geral-vantagens_icone1"></div>
                    {{$vantagens->feature1}}
                </div>
                <div class="vantagens">
                    <div class="geral-vantagens_icone2"></div>
                    {{$vantagens->feature2}}
                </div>
                <div class="vantagens">
                    <div class="geral-vantagens_icone3"></div>
                    {{$vantagens->feature3}}
                </div>
            </div>
            <div class="direita">
                <div class="vantagens">
                    <div class="geral-vantagens_icone4"></div>
                    {{$vantagens->feature4}}
                </div>
                <div class="vantagens">
                    <div class="geral-vantagens_icone5"></div>
                    {{$vantagens->feature5}}
                </div>
                <div class="vantagens">
                    <div class="geral-vantagens_icone6"></div>
                    {{$vantagens->feature6}}
                </div>
            </div>
        </div>
    </div>
</section>

<section id="comerciante">
    <div class="centro">
        <h3>{{$comerciante->titulo}}</h3>
        <div class="macbook"></div>
        <div class="beneficios">
            <ul>
                <li class="beneficios1">{{$comerciante->beneficio1}}</li>
                <li class="beneficios2">{{$comerciante->beneficio2}}</li>
                <li class="beneficios3">{{$comerciante->beneficio3}}</li>
                <li class="beneficios4">{{$comerciante->beneficio4}}</li>
                <li class="beneficios5">{{$comerciante->beneficio5}}</li>
            </ul>
            <a href="comerciante" class="botao abreOverlay" title="saiba o que o GIKS pode fazer pela sua empresa">{{$comerciante->texto_botao}} »</a>
        </div>
    </div>
</section>

<section id="quem-aderiu">
    <!--<div class="centro">
        <h3>Conheça as empresas que já aderiram ao GIKS
            <ul>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
                <li><a href="#">
                    <img src="assets/img/empresas/trupe-off.png">
                    <img src="assets/img/empresas/trupe-on.png">
                </a></li>
            </ul>
        </h3>
    </div>-->
</section>

<section id="contato">
    <div class="icone-contato"></div>
    <div class="centro">
        <div class="formulario">
            <h3>{{$contato->titulo}}</h3>
            <form action="" onsubmit="return envioContato()">
                <input name="nome" id="contatoVisitantesNome" type="text" placeholder="Nome" required>
                <input name="email" id="contatoVisitantesEmail" type="email" placeholder="E-mail" required>
                <input name="assunto" id="contatoVisitantesAssunto" type="text" placeholder="Assunto">
                <textarea name="mensagem" id="contatoVisitantesMensagem" placeholder="Mensagem" required></textarea>
                <input type="submit" value="Enviar mensagem">
            </form>
        </div>
        <div class="response">
            <h5>Sua mensagem foi enviada com sucesso!</h5>
            <p>Em breve entraremos em contato.</p>
        </div>
    </div>
</section>

<div id="newsletter">
    <div class="pattern"></div>
    <div class="centro">
        <div class="formulario">
            <h4>Inscreva-se em nossa newsletter</h4>
            <form action="" onsubmit="return envioNewsletter()">
                <input name="nome" id="newsletterNome" type="text" placeholder="Nome" required>
                <input name="email" id="newsletterEmail" type="email" placeholder="E-mail" required>
                <input type="submit" value="Enviar »">
            </form>
        </div>
        <div class="response">
            <h4>E-mail cadastrado com sucesso!</h4>
        </div>
    </div>
</div>

<footer>
    <div class="centro">
        <h3>Giks</h3>
        <div class="social">
            <ul>
            	@if($contato->facebook)
            		<li><a href="{{$contato->facebook}}" target="_blank" class="geral-social_facebook" title="Facebook">Facebook</a></li>
            	@endif
            	@if($contato->twitter)
            		<li><a href="{{$contato->twitter}}" target="_blank" class="geral-social_twitter" title="Twitter">Twitter</a></li>
            	@endif
            	@if($contato->instagram)
            		<li><a href="{{$contato->instagram}}" target="_blank" class="geral-social_instagram" title="Instagram">Instagram</a></li>
            	@endif
            	@if($contato->google_plus)
            		<li><a href="{{$contato->google_plus}}" target="_blank" class="geral-social_gplus" title="Google+">Google+</a></li>
            	@endif
            	@if($contato->youtube)
            		<li><a href="{{$contato->youtube}}" target="_blank" class="geral-social_youtube" title="YouTube">YouTube</a></li>
            	@endif
            	@if($contato->vimeo)
            		<li><a href="{{$contato->vimeo}}" target="_blank" class="geral-social_vimeo" title="Vimeo">Vimeo</a></li>
            	@endif
            </ul>
        </div>
        <!-- <a href="termos-de-uso" class="abreOverlay botao-termos">Termos de Uso</a> -->
        <p class="copyright">© Giks. Todos os direitos reservados. <a href="http://www.trupe.net" target="_blank" title="Criação de Sites: Trupe Agência Criativa">Criação de Sites: Trupe Agência Criativa</a></p>
    </div>
</footer>