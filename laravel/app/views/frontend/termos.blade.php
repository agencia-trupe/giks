<div class="overlay">
    <div class="centro">
        <a href="home" class="fechaOverlay overlay-voltar">Voltar</a>
        <div class="overlay-conteudo overlay-termos">
            <h1>Termos de uso</h1>
            <div class="esquerda">
                {{$termos->texto_coluna_esq}}
            </div>
            <div class="direita">
                {{$termos->texto_coluna_dir}}
            </div>
        </div>
    </div>
</div>