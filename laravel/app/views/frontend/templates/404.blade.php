<!doctype html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="robots" content="index, follow" />
        <title>{{$seo->titulo}}</title>
        <meta name="description" content="{{$seo->descricao}}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="keywords" content="{{$seo->keywords}}" />
		<meta property="og:title" content="{{$seo->titulo}}"/>
		<meta property="og:description" content="{{$seo->descricao}}"/>
	    <meta property="og:site_name" content="Giks"/>
	    <meta property="og:type" content="website"/>
	    <meta property="og:image" content="assets/img/seo/{{$seo->imagem_facebook}}"/>
	    <meta property="og:url" content="{{ Request::url() }}"/>

	    <base href="{{ url() }}/">
		<script>var BASE = "{{ url() }}"</script>

        <link rel="stylesheet" href="assets/css/main.min.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,700|Roboto+Condensed:300' rel='stylesheet' type='text/css'>

    </head>
	<body>
        <div id="menu" class="notfound">
            <div class="centro">
                <h1><a href="home">Giks</a></h1>                
            </div>
        </div>
        <section id="intro">
            <div class="centro" style="text-align:center;">
                <h2 style="width:100%;">Página não encontrada</h2>
                <a href="home" style="color:#fff; font-family: 'Roboto', sans-serif; font-size: 16px;" title="voltar">&larr; voltar</a>
            </div>
        </section>	

        <footer>
            <div class="centro">
                <h3>Giks</h3>
                <div class="social">
                    <ul>
                    	@if($contato->facebook)
                    		<li><a href="{{$contato->facebook}}" target="_blank" class="geral-social_facebook" title="Facebook">Facebook</a></li>
                    	@endif
                    	@if($contato->twitter)
                    		<li><a href="{{$contato->twitter}}" target="_blank" class="geral-social_twitter" title="Twitter">Twitter</a></li>
                    	@endif
                    	@if($contato->instagram)
                    		<li><a href="{{$contato->instagram}}" target="_blank" class="geral-social_instagram" title="Instagram">Instagram</a></li>
                    	@endif
                    	@if($contato->google_plus)
                    		<li><a href="{{$contato->google_plus}}" target="_blank" class="geral-social_gplus" title="Google+">Google+</a></li>
                    	@endif
                    	@if($contato->youtube)
                    		<li><a href="{{$contato->youtube}}" target="_blank" class="geral-social_youtube" title="YouTube">YouTube</a></li>
                    	@endif
                    	@if($contato->vimeo)
                    		<li><a href="{{$contato->vimeo}}" target="_blank" class="geral-social_vimeo" title="Vimeo">Vimeo</a></li>
                    	@endif
                    </ul>
                </div>
                <a class="botao-termos">Termos de Uso</a>
                <p class="copyright">© Giks. Todos os direitos reservados. Criação de Sites: <a href="#">Trupe Agência Criativa</a></p>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
        <script src="assets/js/main.min.js"></script>        
	</body>
</html>