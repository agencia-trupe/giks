<!doctype html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="robots" content="index, follow" />
        <title>{{$seo->titulo}}</title>
        <meta name="description" content="{{$seo->descricao}}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="keywords" content="{{$seo->keywords}}" />
		<meta property="og:title" content="{{$seo->titulo}}"/>
		<meta property="og:description" content="{{$seo->descricao}}"/>
	    <meta property="og:site_name" content="Giks"/>
	    <meta property="og:type" content="website"/>
	    <meta property="og:image" content="assets/img/seo/{{$seo->imagem_facebook}}"/>
	    <meta property="og:url" content="{{ Request::url() }}"/>

	    <base href="{{ url() }}/">
		<script>var BASE = "{{ url() }}"</script>

        <link rel="stylesheet" href="assets/css/main.min.css">
        <link href='http://fonts.googleapis.com/css?family=Roboto:100,300,700|Roboto+Condensed:300' rel='stylesheet' type='text/css'>

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-XXXXXXXX-X', 'auto');
		  ga('send', 'pageview');

		</script>
    </head>
    <body>
		
		@include('frontend.index')

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/jquery.min.js"><\/script>')</script>
        <script src="assets/js/main.min.js"></script>
    </body>
</html>
