<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersSeeder');
		$this->call('BannerSeeder');
		$this->call('SobreSeeder');
		$this->call('ComoFuncionaSeeder');
		$this->call('VantagensSeeder');
		$this->call('ComercianteSeeder');
		$this->call('PelaSuaEmpresaSeeder');
		$this->call('ContatoSeeder');
		$this->call('TermosDeUsoSeeder');
		$this->call('SEOSeeder');
		$this->call('LinksSeeder');
	}

}
