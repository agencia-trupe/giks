<?php

class VantagensSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'frase_principal' => '',
				'frase_secundaria' => '',
				'feature1' => '',
				'feature2' => '',
				'feature3' => '',
				'feature4' => '',
				'feature5' => '',
				'feature6' => ''
            )
        );

        DB::table('vantagens')->insert($data);
    }

}
