<?php

class PelaSuaEmpresaSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'titulo' => '',
				'destaque' => '',
				'texto' => '',
				'chamada_formulario' => '',
            )
        );

        DB::table('pela_sua_empresa')->insert($data);
    }

}
