<?php

class ContatoSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'titulo' => '',
				'email_contato_visitantes' => '',
				'email_contato_empresas' => '',
				'facebook' => '',
				'twitter' => '',
				'instagram' => '',
				'google_plus' => '',
				'youtube' => '',
				'vimeo' => ''
            )
        );

        DB::table('contato')->insert($data);
    }

}
