<?php

class ComoFuncionaSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
				'frase_principal' => '',
				'frase_secundaria' => '',
				'texto_coluna_esq' => '',
				'texto_coluna_dir' => '',
				'passo1' => '',
				'passo2' => '',
				'passo3' => '',
				'passo4' => ''
            )
        );

        DB::table('como_funciona')->insert($data);
    }

}
