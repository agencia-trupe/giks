<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelaSuaEmpresaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pela_sua_empresa', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('titulo');
			$table->text('destaque');
			$table->text('texto');
			$table->text('chamada_formulario');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pela_sua_empresa');
	}

}
