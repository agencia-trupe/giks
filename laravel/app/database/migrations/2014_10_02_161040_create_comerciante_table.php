<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComercianteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comerciante', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('titulo');			
			$table->text('beneficio1');
			$table->text('beneficio2');
			$table->text('beneficio3');
			$table->text('beneficio4');
			$table->text('beneficio5');
			$table->text('texto_botao');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comerciante');
	}

}
