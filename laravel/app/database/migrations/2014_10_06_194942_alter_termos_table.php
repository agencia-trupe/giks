<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTermosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('termos_de_uso', function(Blueprint $table)
		{
			$table->dropColumn('texto');
			$table->text('texto_coluna_esq')->after('id');
			$table->text('texto_coluna_dir')->after('texto_coluna_esq');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('termos_de_uso', function(Blueprint $table)
		{
			$table->dropColumn('texto_coluna_esq');
			$table->dropColumn('texto_coluna_dir');
			$table->text('texto')->after('id');
		});
	}

}
