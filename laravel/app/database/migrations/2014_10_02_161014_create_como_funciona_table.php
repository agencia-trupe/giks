<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComoFuncionaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('como_funciona', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('frase_principal');
			$table->text('frase_secundaria');
			$table->text('texto_coluna_esq');
			$table->text('texto_coluna_dir');
			$table->text('passo1');
			$table->text('passo2');
			$table->text('passo3');
			$table->text('passo4');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('como_funciona');
	}

}
