<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatosEmpresasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contatos_empresas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('responsavel');
			$table->string('email');
			$table->string('empresa');
			$table->string('atuacao');
			$table->string('telefone');
			$table->text('mensagem');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contatos_empresas');
	}

}
