<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contato', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('titulo');
			$table->text('email_contato_visitantes');
			$table->text('email_contato_empresas');
			$table->text('facebook');
			$table->text('twitter');
			$table->text('instagram');
			$table->text('google_plus');
			$table->text('youtube');
			$table->text('vimeo');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contato');
	}

}
