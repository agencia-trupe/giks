<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVantagensTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vantagens', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('frase_principal');
			$table->text('frase_secundaria');
			$table->text('feature1');
			$table->text('feature2');
			$table->text('feature3');
			$table->text('feature4');
			$table->text('feature5');
			$table->text('feature6');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vantagens');
	}

}
