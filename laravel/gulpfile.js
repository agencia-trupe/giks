var gulp = require('gulp'); 

var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minifycss = require('gulp-minify-css');

gulp.task('lint', function() {
    return gulp.src('../public/assets/js/main.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('scripts', function() {
    return gulp.src('../public/assets/js/main.js')
        .pipe(uglify())
        .pipe(rename('main.min.js'))
        .pipe(gulp.dest('../public/assets/js/'));
});

gulp.task('styles', function() {
    return gulp.src('../public/assets/css/main.css')
    	.pipe(minifycss())
    	.pipe(rename('main.min.css'))
        .pipe(gulp.dest('../public/assets/css/'));
});

gulp.task('default', ['lint', 'scripts', 'styles']);
gulp.task('js', ['scripts']);